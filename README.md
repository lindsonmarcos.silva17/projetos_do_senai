
# 😎 Apresentando Lindson Marcos
- Cursei Programação de Jogos Digitais;
- Estudei a linguagem de programação C#.

# Meus projetos 👋

### Casa dos Arrepios 🏠
Esse projeto está relacionado ao período em que estudei no SENAI.

[◾ Casa dos Arrepios (Itch.io)](https://lindson-brandao34.itch.io/casa-dos-arrepios)

### Office Belon 📰
Trata-se de um projeto 2D que se passa em Carandina, a “cidade do caos”, um lugar onde as fake news disseminam-se sem controle. Foi em meio a essa situação difícil que surgiu a agência Office Belon que contratou o misterioso investigador Davi Freitas. O jogo busca ensinar a combater as notícias falsas através de uma gameplay simples e divertida.

Autores:
Beatriz Luize, Cássio Gabriel, Lindson Marcos, Pedro Pardini e Rodrigo Grimaldi

[◾ Office Belon (Itch.io)](https://cssomc.itch.io/office-belon)
## Screenshots

_Screenshot Casa dos Arrepios_
![App Screenshot](https://img.itch.zone/aW1hZ2UvMjQwOTQwMC8xNDI2MDc4MS5wbmc=/original/JOlp5h.png)

_Screenshot Office Belon_
![App Screenshot](https://img.itch.zone/aW1hZ2UvMjM5NzUxMi8xNDI2MDcxNS5wbmc=/original/7YQQAO.png)



## 🔗 Links
[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/lindson-brand%C3%A3o-50a0ab2a3/)

